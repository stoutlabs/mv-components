import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

import Fractions from "./components/Fractions";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Quick Component Tester!</h1>
        </header>
        <div className="components">
          <h2>Fractions Simplifier</h2>
          <Fractions />
        </div>
      </div>
    );
  }
}

export default App;
