import React, { Component } from "react";
import styled from "styled-components";

const StyleFractions = styled.div`
  --fraction-text-color: #222;

  background: #e6e6e6;
  border: 1px solid #888;
  border-radius: 5px;
  margin: 0 auto;
  max-width: 500px;
  padding: 2rem;
  width: auto;

  div.fractions-main {
    align-items: center;
    display: flex;
    flex-direction: row;
    font-weight: bold;
    justify-content: center;
    padding: 0 0 1rem;

    div.inputs {
      padding: 0 1rem;

      input {
        border: 1px solid #888;
        color: var(--fraction-text-color);
        display: block;
        font-size: 1.4rem;
        margin: 0 auto;
        padding: 0.4rem 0.2rem;
        text-align: center;
        width: 60px;

        &:first-child {
          border-bottom: 3px solid #666;
          margin: 0 0 0.2rem;
        }
      }
    }

    span.equals {
      display: inline-block;
      font-size: 1.6rem;
    }

    div.new-fraction {
      margin: 0 0 0 1rem;

      sup,
      sub {
        display: block;
        font-size: 1.5rem;
        margin: 0;
        min-width: 50px;
        padding: 0.4rem 0;
      }

      sup {
        border-bottom: 2px solid var(--fraction-text-color);
        margin: 0 0 0.2rem;
      }
    }
  }

  div.fractions-info {
    padding: 1.5rem 0 0;

    p.error {
      color: red;
    }
    button {
      background-color: #00adbb;
      border: none;
      color: #fff;
      font-size: 1rem;
      font-weight: bold;
      outline: none;
      padding: 0.75rem;
      transition: background-color 250ms ease-out;

      &:hover {
        background-color: #007e88;
      }
    }

    div.explain {
      padding: 1.5rem 0 0;

      h4 {
        font-size: 1.3rem;
        margin: 0;
        padding: 0;
      }

      p {
        font-size: 1rem;
        line-height: 1.5;
        letter-spacing: 0.01rem;
      }
    }
  }
`;

export class Fractions extends Component {
  state = {
    numerator: "",
    denominator: "",
    newNumerator: "",
    newDenominator: "",
    gcd: "",
    positive: true,
    error: false
  };

  handleChange = e => {
    let newText = "";
    const numbers = "0123456789";
    const text = e.target.value;

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      }
    }

    this.setState({ [e.target.name]: newText, gcd: "" });
  };

  // obtain greatest common denominator
  getGCD = (numerator, denominator) => {
    return denominator === 0 ? numerator : this.getGCD(denominator, numerator % denominator);
  };

  simplifyFraction = () => {
    // edge case: check if denominator is 0
    if (Number(this.state.denominator) === 0) {
      this.setState({ error: "Denominator cannot be zero." });
      return;
    }

    this.setState({ error: "" });
    const theGCD = this.getGCD(this.state.numerator, this.state.denominator);
    let newNumerator = this.state.numerator / theGCD;
    let newDenominator = this.state.denominator / theGCD;
    let positive = true;

    // handle negative cases
    if ((newNumerator < 0 && newDenominator > 0) || (newNumerator > 0 && newDenominator < 0)) {
      positive = false;
      newNumerator = Math.abs(newNumerator);
      newDenominator = Math.abs(newDenominator);
    }

    this.setState({ newNumerator, newDenominator, positive, gcd: theGCD });
  };

  renderExplanation = () => {
    const explanationMessage =
      this.state.gcd === 1 ? (
        <div className="explain">
          <h4>Explanation:</h4>
          <p>
            Because the numerator and denominator do not share a common divisor, this fraction
            cannot be further simplified.
          </p>
        </div>
      ) : this.state.gcd !== "" ? (
        <div className="explain">
          <h4>Explanation:</h4>
          <p>
            The numerator and denominator's greatest common divisor is {this.state.gcd}, so we can
            simplify the fraction by dividing them both by this number.
          </p>
          <p>
            The new numerator is ({this.state.numerator} &#247; {this.state.gcd}
            ), which is {this.state.newNumerator}.
          </p>
          <p>
            The new denominator is ({this.state.denominator} &#247; {this.state.gcd}
            ), which is {this.state.newDenominator}.
          </p>
        </div>
      ) : (
        ""
      );

    return explanationMessage;
  };

  render() {
    const { numerator, denominator } = this.state;
    const buttonEnabled = numerator !== "" && denominator !== "";

    return (
      <StyleFractions>
        <p>(Instructions here?)</p>
        <div className="fractions-main">
          <div className="inputs">
            <input
              name="numerator"
              type="text"
              value={numerator}
              onChange={this.handleChange}
              placeholder="?"
            />
            <input
              name="denominator"
              type="text"
              value={denominator}
              onChange={this.handleChange}
              placeholder="?"
            />
          </div>

          <span className="equals">=</span>

          <div className="new-fraction">
            <sup className="new-numerator">
              {this.state.positive ? this.state.newNumerator : `- ${this.state.newNumerator}`}
            </sup>
            <sub className="new-denominator">{this.state.newDenominator}</sub>
          </div>
        </div>

        <div className="fractions-info">
          <button onClick={this.simplifyFraction} disabled={!buttonEnabled}>
            Simplify
          </button>
          {this.state.error !== "" && <p className="error">{this.state.error}</p>}
          {this.renderExplanation()}
        </div>
      </StyleFractions>
    );
  }
}

export default Fractions;
